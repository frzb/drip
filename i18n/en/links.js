export default {
  gitlab: {
    url: 'https://gitlab.com/bloodyhealth/drip',
    text: 'GitLab'
  },
  email: {
    url: 'mailto:bloodyhealth@mailbox.org',
    text: 'email'
  },
  wiki: {
    url: 'https://gitlab.com/bloodyhealth/drip/wikis/home',
    text: 'wiki'
  },
  website: {
    url: 'https://bloodyhealth.gitlab.io/'
  },
  moreAboutNfp: {
    url: 'https://gitlab.com/bloodyhealth/drip/wikis/nfp/intro',
    text: 'More'
  },
}
